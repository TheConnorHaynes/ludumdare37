#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying vec2 v_texCoords;
uniform vec3 hair;
uniform sampler2D u_texture;
void main()
{
    vec4 color = texture2D(u_texture,v_texCoords);

    vec4 black = vec4(0,0,0,1);
    if(color.rgb == black.rgb)
        color.rgb = hair;


	gl_FragColor = color ;
}