#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

uniform float iGlobalTime;
varying vec2 v_texCoords;
uniform vec3 hair;
uniform sampler2D u_texture;
void main()
{
    vec4 color = texture2D(u_texture,v_texCoords);

    vec4 white = vec4(1,1,1,1);
    if(color.rgb == white.rgb)
        color.rgb = hair;

    float time = iGlobalTime;
    for(float i = .6; i < 35.; i++){
        if(sin((v_texCoords.x+time+i*5.0)*200.0)/200.0+v_texCoords.y > i*.006 && sin((v_texCoords.x+time+i*5.)*200.0)/200.0+v_texCoords.y < i*.006+.001){
            color.r/=2.0;
            color.b = .25;
            color.g = .5;
        }
    }
    color.r = color.r-color.r/7.;
    color.b +=.1;
    vec2 center = vec2(.5,.5);
    //color = color - .5*color*distance(center,v_texCoords);




	gl_FragColor = color ;
}