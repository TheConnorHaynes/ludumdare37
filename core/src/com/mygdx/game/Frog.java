package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by hcmg_local on 12/10/2016.
 */
public class Frog extends MovingObject{
    float drag;
    private final float HOPSPEED = 1000/60f;
    TextureRegion[][] sheet;
    TextureRegion sprite;
    Animation animation;
    Animation up;
    Animation down;
    Animation left;
    Animation right;
    float time;
    boolean hopping;
    Sound croak;
    Sound caught;

    public Frog(int x, int y){
        super(x,y,64,64);
        caught = Gdx.audio.newSound(Gdx.files.internal("Sounds/Screaming Frog.wav"));
        croak = Gdx.audio.newSound(Gdx.files.internal("Sounds/Frog Croaking-SoundBible.com-1053984354.wav"));
        time = 0;
        sheet = new TextureRegion(new Texture("frog.png")).split(64,64);
        drag = 10/60f;
        sprite = sheet[0][0];
        animation = new Animation(.1f,sheet[0]);
        up = new Animation(.1f, sheet[1]);
        down = new Animation(.1f, sheet[0]);
        right = new Animation(.1f, sheet[2]);
        left = new Animation(.1f, sheet[3]);
        hopping = false;
        superHop();

    }
    public void superHop(){
        if(!MainGame.muted)
            croak.play(.2f);
        hopping = true;
        dx = ((float)(Math.random())-.5f)*HOPSPEED*3;
        dy = ((float)(Math.random())-.5f)*HOPSPEED*3;
        if(Math.abs(dx)>Math.abs(dy)) {
            if (dx > 0) {
                animation = right;
            } else if (dx < 0) {
                animation = left;
            }
        }
        else {
            if (dy > 0) {
                animation = up;
            } else animation = down;
        }
        time = 0;
    }

    public void updatePhysics(){
        if(dx == 0 && dy == 0){
            if(Math.random()>.98f)
                hop();
        }
        super.updatePhysics();
        if(dx > 0){
            if(dx<drag)
                dx=0;
            else
                dx -= drag;
        }
        if(dx < 0){
            if(dx>drag)
                dx=0;
            else
                dx += drag;
        }
        if(dy > 0){
            if(dy<drag)
                dy=0;
            else
                dy -= drag;
        }
        if(dy < 0){
            if(dy>drag)
                dy=0;
            else
                dy += drag;
        }
    }

    public void hop(){
        if(!MainGame.muted)
            croak.play(.2f);
        hopping = true;
        dx = ((float)(Math.random())-.5f)*HOPSPEED;
        dy = ((float)(Math.random())-.5f)*HOPSPEED;
        if(Math.abs(dx)>Math.abs(dy)) {
            if (dx > 0) {
                animation = right;
            } else if (dx < 0) {
                animation = left;
            }
        }
        else {
            if (dy > 0) {
                animation = up;
            } else animation = down;
        }
        time = 0;
    }

    public void render(SpriteBatch batch, float delta, World world){
        sprite = animation.getKeyFrame(time);
        batch.draw(sprite,x,y);
        if(collides(world.player)){
            if(!MainGame.muted)
                caught.play(.2f);
            world.frogs.removeValue(this, true);
        }
        if(time < .6f && hopping){
            time+=delta;
        }
        if(time > .6f){
            hopping = false;
            time = 0;
        }
    }

    public boolean equals(Object o){
        if(o instanceof Frog && ((Frog) o).x == x && ((Frog) o).y == y && sprite.equals(((Frog) o).sprite)){
            return true;
        }
        return false;
    }
}
