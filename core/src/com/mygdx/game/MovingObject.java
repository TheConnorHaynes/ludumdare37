package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Connor on 5/10/2016.
 */
public class MovingObject implements Json.Serializable {
    protected float x;
    protected float y;
    protected float dx;
    protected float dy;
    protected float speed;
    protected float height;
    protected float width;




    public MovingObject(float x, float y, float width, float height){
        this.x = x;
        this.y = y;
        this.width = width;
        this. height = height;
        dx = 0;
        dy = 0;
        speed = 10;
    }

    public void updatePhysics(){
        move(MainGame.world.foreground);
    }

    public MovingObject(){
        dx = 0;
        dy = 0;
    }

    public void walk(float x, float y){
        if(x > 0)
            dx += speed*1/60f;
        else if(x < 0)
            dx += -speed*1/60f;

        if(y > 0)
            dy += speed*1/60f;
        else if(y < 0)
            dy += -speed*1/60f;
    }

    public void reset(){
        dx = 0;
        dy = 0;
    }

    public void move(Array<Tile> tiles){
        x+=dx;
        for(Tile t : tiles){
            x -= t.collidex(this);
        }

        y+=dy;
        for(Tile t : tiles){
            y -= t.collidey(this);

        }
        if(x > 800-width-55)
            x = 800 - width-55;
        if(x < 55)
            x = 55;
        if(y > 600-height-170)
            y = 600 - height-170;
        if(y < 0)
            y = 0;
    }


    public boolean equals(Object o){
        boolean ret = false;
        if(o instanceof MovingObject) {
            if (((MovingObject) o).x == x && ((MovingObject) o).y == y)
                ret = true;
        }
        return ret;
    }

    public void kill(){

    }

    public void render(SpriteBatch batch, float delta, World world){

    }




    @Override
    public void write(Json json) {
        json.writeValue("x", x);
        json.writeValue("y", y);
        json.writeValue("width", width);
        json.writeValue("height", height);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        x = jsonData.getFloat("x");
        y = jsonData.getFloat("y");
        width = jsonData.getFloat("width");
        height = jsonData.getFloat("height");
    }



    public boolean collides(MovingObject o){
        return (x < o.x+o.width && x+width > o.x && y < o.y+o.height && y+height > o.y);
    }
}
