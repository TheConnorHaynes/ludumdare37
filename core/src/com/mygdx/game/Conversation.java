package com.mygdx.game;

import com.badlogic.gdx.utils.Array;
import java.util.HashMap;

public class Conversation {

    public String action;
    HashMap<Integer, Node> nodes;
    public Node currNode;
    Personality personality;
    int start;

    public Conversation(String action, int start, Array<Node> nodeList, Personality personality) {
        this.action = action;
        this.nodes = new HashMap<Integer, Node>();
        for (Node node : nodeList) {
            nodes.put(node.id, node);
        }
        this.start = start;
        this.currNode = getNode(start);
        this.personality = personality;
    }

    public Node getNode(int id) {
        return nodes.get(id);
    }

    public Array<String> getActions() {
        return currNode.getActionsText();
    }

    public Tuple<String, Integer> takeAction(String action) {
        Tuple<Integer, Integer> response = currNode.takeAction(action);
        currNode = nodes.get(response.x);
        if (currNode != null)
            return new Tuple<String, Integer>(currNode.text, response.y);
        else {
            currNode = getNode(start);
            return new Tuple<String, Integer>(null, response.y);
        }
    }

}
