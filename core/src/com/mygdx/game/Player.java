package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


/**
 * Created by Connor on 12/10/2016.
 */
public class Player extends MovingObject {

    TextureRegion sprite;
    public Animation animation;
    Animation up;
    Animation down;
    Animation right;
    Animation left;
    Animation walkUp;
    Animation walkDown;
    Animation walkRight;
    Animation walkLeft;
    Animation push;
    TextureRegion[][] spriteSheet;

    float time;
    float animEnd;

    public Player(){
        super(800/2, 600/2-100, 64, 64);
        animEnd = .6f;
        speed = 180;
        spriteSheet = new TextureRegion(new Texture("playersheet.png")).split(64,64);
        walkDown = new Animation(.1f, spriteSheet[0]);
        walkRight = new Animation(.1f, spriteSheet[2]);
        walkLeft = new Animation(.1f, spriteSheet[3]);
        walkUp = new Animation(.1f, spriteSheet[1]);
        //TODO update these
        up = walkUp;
        down = walkDown;
        right = walkRight;
        left = walkLeft;
        animation = walkDown;
        push = new Animation(.1f, spriteSheet[4]);

        animEnd = .6f;
        time = 0;

    }

    public void updatePhysics(){
        if(!animation.equals(push))
            super.updatePhysics();
    }

    public void interact(World world){
        Interact i;

        i = new Interact((int)(x+width/2), (int)(y+height+10));

        animation = push;
        animEnd = .4f;

        for(Tile t : world.foreground){
            if(t instanceof Button){
                t.collidex(i);
            }
        }

    }

    public void render(SpriteBatch batch, float delta, World world){

        if(dx > 0)
            animation = walkRight;
        else if(dx < 0){
            animation = walkLeft;
        }
        else if(dy > 0)
            animation = walkUp;
        else if(dy<0)
            animation = walkDown;

        if(animation == walkRight && dx == 0){
            animation = right;
            animEnd = .1f;
        }
        if(animation == walkLeft && dx == 0){
            animation = left;
            animEnd = .1f;

        }
        if(animation == walkUp && dy == 0){
            animation = up;
            animEnd = .1f;
        }
        if(animation == walkDown && dy == 0){
            animation = down;
            animEnd = .1f;
        }


        time += delta;
        if(time > animEnd) {
            if(animation.equals(push)) {
                animation = up;
                animEnd = .6f;
            }
            time = 0;
        }
        sprite = animation.getKeyFrame(time);
        batch.draw(sprite, x, y);
    }

}
