package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Connor on 12/10/2016.
 */
public class MainGame implements Screen {

    public static final String credits = "Programmers: Connor Haynes and Ryan Kubik --- Dialogue Writers: Tristan Snyder and Jordan Hallow --- Audio Engineer: Curtis Stapleton --- Artist: Gabby Dumire";

    private Sound ding;
    private int choice;
    public static boolean menu;
    public static BitmapFont font;
    public static BitmapFont smallerFont;
    private float shaderTime;
    private float physicsTime;
    public float beatTime;
    public Music music;

    public Viewport view;
    public static Camera cam;

    public static ScrollingText scrollingText;
    public static  boolean creditsRoll;
    public static SpriteBatch batch;

    public static final float WIDTH = 800;
    public static final float HEIGHT = 600;

    public static final float TILE_SIZE = 64;

    public static World world;
    public static TextureRegion[][] splitSheet;

    public FrameBuffer fbo;

    private boolean beat;

    public Player player;

    public static Dialogue dialogue;

    public Sound  heartbeat;

    public static boolean muted;


    @Override
    public void show() {
        muted = false;
        music = Gdx.audio.newMusic(Gdx.files.internal("Sounds/ES_Late Night Dinner 5 - Magnus Ringblom.mp3"));
        music.setVolume(.15f);
        music.play();
        scrollingText = new ScrollingText();
        creditsRoll = false;
        ding = Gdx.audio.newSound(Gdx.files.internal("Sounds/MenuDing.wav"));
        choice = 0;
        menu = true;
        FileHandle font1 = Gdx.files.internal("font/8-BIT_WONDER.fnt");
        FileHandle font2 = Gdx.files.internal("font/8-BIT_WONDER_0.png");
        font = new BitmapFont(Gdx.files.internal("dotfont/dotfont.fnt"), Gdx.files.internal("dotfont/dotfont_0.png"), false);
        //font.getData().setScale(.5f, .5f);
        font.setColor(Color.WHITE);
        smallerFont = new BitmapFont(font1, font2, false);
        smallerFont.getData().setScale(.25f, .25f);

        heartbeat = Gdx.audio.newSound(Gdx.files.internal("Sounds/FinalHeartbeat.wav"));
        fbo = new FrameBuffer(Pixmap.Format.RGB888, 800, 600, false);
        //font = new BitmapFont();
        cam = new OrthographicCamera(WIDTH, HEIGHT);
        cam.position.set(WIDTH/2, HEIGHT/2, 0);
        cam.update();
        view = new FitViewport(WIDTH, HEIGHT, cam);
        batch = new SpriteBatch();

        batch.setProjectionMatrix(cam.combined);
        splitSheet = new TextureRegion(new Texture(Gdx.files.internal("faces.png"))).split(32,32);
        physicsTime = 0;
        world = new World();
        beatTime = 0;
        shaderTime = 0;

        dialogue = new Dialogue();
//        System.out.println("Resp: " + dialogue.startNextConversation());
//        for (String action : dialogue.getActions()) {
//            System.out.print(action + " : ");
//        }
//        System.out.println();
//        System.out.println("Current Node: " + dialogue.currConvo.currNode.id);
//        String reponse = dialogue.takeAction("Flirt");
//        System.out.println("Resp: " + reponse);
//
//        for (String action : dialogue.getActions()) {
//            System.out.print(action + " : ");
//        }
//        System.out.println();
//        System.out.println("Current Node: " + dialogue.currConvo.currNode.id);
//        reponse = dialogue.takeAction("Tease");
//        System.out.println("Resp: " + reponse);
    }

    @Override
    public void render(float delta) {
        shaderTime+=delta;
        if(shaderTime>2*Math.PI)
            shaderTime = 0;
        if(world.beating) {
            beatTime += delta;
            if(beatTime > 1){
                beatTime = 0;
                if(beat){
                    beat = false;
                    beatTime = 0;
                }
                else{
                    beatTime = .5f;
                    beat = true;
                    if(!muted)
                    heartbeat.play();
                }
            }
        }
        fbo.begin();
        if(!menu && !creditsRoll && !(world.win || world.lose))
            physicsTime += delta;
        pollInput();
        if(physicsTime > 1/60f)
            updatePhysics();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        if(!menu){
            scrollingText.render(delta, batch, font);
        }
        world.render(batch, delta);
        batch.end();
        fbo.end();
        batch.begin();
        if(beat)
            batch.draw(fbo.getColorBufferTexture(), -10, 610, 820, -620);
        else
            batch.draw(fbo.getColorBufferTexture(), 0, 600, 800, -600);

        if(menu){
            if(choice == 0) {
                font.setColor(Color.GOLD);
                font.draw(batch, "Start", 200, 430);
                font.setColor(Color.WHITE);
            }
            else
                font.draw(batch, "Start", 200, 430);

            if(choice == 1) {
                font.setColor(Color.GOLD);
                font.draw(batch, "Credits", 350, 430);
                font.setColor(Color.WHITE);
            }
            else
                font.draw(batch, "Credits", 350, 430);

            if(choice == 2) {
                font.setColor(Color.GOLD);
                font.draw(batch, "Exit", 510, 430);
                font.setColor(Color.WHITE);
            }
            else
                font.draw(batch, "Exit", 510, 430);


        }
        batch.end();
    }

    public static void setButtons(){
        Array<String> actions = dialogue.getActions();
        if(actions.size == 1){
            world.buttons.get(2).setText(actions.get(0), Color.WHITE);
            world.buttons.get(0).setText("", Color.WHITE);
            world.buttons.get(1).setText("", Color.WHITE);
        }
        else if(actions.size == 2){
            world.buttons.get(2).setText("", Color.WHITE);
            world.buttons.get(0).setText(actions.get(0), Color.WHITE);
            world.buttons.get(1).setText(actions.get(1), Color.WHITE);
        }
        else{
            world.buttons.get(2).setText(actions.get(0), Color.WHITE);
            world.buttons.get(0).setText(actions.get(1), Color.WHITE);
            world.buttons.get(1).setText(actions.get(2), Color.WHITE);
        }
    }

    public void pollInput(){
        if(Gdx.input.isKeyJustPressed(Input.Keys.M)){
            if(!muted) {
                music.stop();
                muted = true;
            }
            else{
                music.play();
                muted = false;
            }
        }
        if(!menu && !creditsRoll) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
                world.player.interact(world);
            if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {
                if (beat)
                    beat = false;
                else
                    beat = true;
            }
        }
        else if(!creditsRoll) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT) || Gdx.input.isKeyJustPressed(Input.Keys.D)) {
                if(!muted)
                 ding.play();
                world.date = new Date();
                if (choice < 2)
                    choice++;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT) || Gdx.input.isKeyJustPressed(Input.Keys.A)) {
                if(!muted)
                    ding.play();
                world.date = new Date();
                if (choice > 0)
                    choice--;
            }


            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                if(!muted)
                    ding.play();
                if (choice == 0) {
                    menu = false;
                    scrollingText.setText(dialogue.startNextConversation());
                    setButtons();
                } else if (choice == 2) {
                    Gdx.app.exit();
                }
                else{
                    creditsRoll = true;
                    scrollingText.setText(credits);
                    menu = false;
                }
            }
        }
        else{
            if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                creditsRoll = false;
                menu = true;
                scrollingText.setText("");
            }
        }
    }

    public void updatePhysics(){
        while(physicsTime > 1/60f) {
            world.player.reset();
            physicsTime -= 1/60f;
            world.player.walk(0,0);
            physicsInput();
            world.updatePhysics();
        }
    }

    @Override
    public void resize(int width, int height) {
        view.update(width, height);
    }

    public void physicsInput(){
        if(Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)){
            world.player.walk(0,1);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            world.player.walk(0,-1);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            world.player.walk(1,0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            world.player.walk(-1,0);
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            menu = true;
            world.reset();
            dialogue = new Dialogue();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
