package com.mygdx.game;

import com.badlogic.gdx.utils.Array;
import java.util.HashMap;
import java.util.Random;

public class Personality {

    Random rand;
    HashMap<String, Trait> traits;

    public Personality(Array<Array<String>> traitGroups) {
        rand = new Random();
        traits = new HashMap<String, Trait>();

        for (Array<String> group : traitGroups) {
            for (String traitName : group) {
                traits.put(traitName, new Trait(group));
            }
        }
    }

    public Tuple<Integer, Integer> takeAction(String action, Choice choice) {
        Trait trait = traits.get(action);
        //randomly like or dislike
        if (rand.nextFloat() <= trait.successChance) {
            trait.successChance += .4f;
            for (String related : trait.relatedTraits) {
                traits.get(related).successChance += .25f;
            }
            System.out.println(choice);
            return new Tuple<Integer, Integer> (choice.like, 1);
        } else {
            trait.successChance -= .4f;
            for (String related : trait.relatedTraits) {
                traits.get(related).successChance -= .25f;
            }
            System.out.println(choice);
            return new Tuple<Integer, Integer> (choice.dislike, 0);
        }
    }

    private class Trait {
        public float successChance;
        public Array<String> relatedTraits;

        public Trait(Array<String> relatedTraits) {
            successChance = 0.5f;
            this.relatedTraits = relatedTraits;
        }
    }

}
