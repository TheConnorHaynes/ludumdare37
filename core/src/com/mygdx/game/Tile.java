package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Connor on 5/10/2016.
 */
public class Tile implements Json.Serializable{
    protected  float height = MainGame.TILE_SIZE;
    protected  float width = MainGame.TILE_SIZE;
    protected float x;
    protected float y;
    //short for sprite X and Y!
    protected int sx;
    protected int sy;

    protected boolean flipx;
    protected boolean flipy;
    //guess!
    public TextureRegion texture;

    @Override
    public void write(Json json) {
        json.writeValue("x", x);
        json.writeValue("y", y);
        json.writeValue("sx", sx);
        json.writeValue("sy", sy);
        json.writeValue("flipx", flipx);
        json.writeValue("flipy", flipy);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        x = jsonData.getInt("x");
        y = jsonData.getInt("y");
        sx = jsonData.getInt("sx");
        sy = jsonData.getInt("sy");
        flipx = jsonData.getBoolean("flipx");
        flipy = jsonData.getBoolean("flipy");
    }

    public Tile(int x, int y, int sx, int sy, boolean flipx, boolean flipy){
        this.x = x;
        this.y = y;
        this.sx = sx;
        this.sy = sy;
        //this is where it is on the main textursheet named groundspritesheet
        texture = new TextureRegion(MainGame.splitSheet[sx][sy]);
        this.flipx = flipx;
        this.flipy = flipy;
        texture.flip(this.flipx, this.flipy);
    }
    public Tile(){

    }

    //this method has to run to make the level saving stuff work
    public void setTexture(){
        texture = new TextureRegion(MainGame.splitSheet[sx][sy]);
        texture.flip(flipx, flipy);
    }


    public float getX(MovingObject o){return x;}

    public float getY(MovingObject o){return y;}

    public float getWidth(MovingObject o){return width;}

    public float getHeight(MovingObject o){return height;}

    public Tile clone(int x, int y, boolean fx, boolean fy){
        Tile c = new Tile(x, y, sx, sy, fx, fy);
        return c;
    }

    //arcadey collisions
    public float collidex(MovingObject o){
        float col = 0;
        if(o.x + o.width > getX(o) && o.x <= getX(o) + getWidth(o) && o.y + o.height - 5 >= getY(o) && o.y + o.height/2f <= getY(o)+getHeight(o)){
            if (o.x + o.width - getX(o) > getX(o) + getWidth(o) - x) {
                col = o.x - (getX(o) + getWidth(o));
            } else {
                col = o.x + o.width - getX(o);
            }
        }
        return col;
    }

    public float collidey(MovingObject o){
        float col = 0;
        if(o.x+o.width*3f/4f >= getX(o) && o.x + o.width/4f <=getX(o)+getWidth(o) && o.y+o.height >= getY(o) && o.y <= getY(o)+getHeight(o) ){
            if (o.y + o.height - getY(o) > getY(o) + getHeight(o) - o.y) {
                col = o.y - (getY(o) + getHeight(o));
                if(o.dy < 0)
                    o.dy = 0;
            } else
                col = o.y + o.height - getY(o);
            }
        return col;
        }



    @Override
    public boolean equals(Object o){
        if(o instanceof Tile){
            Tile t = (Tile)o;
            if(x == t.x)
                if(y == t.y)
                    if(sx == t.sx)
                        if(sy == t.sy)
                            if(flipx == t.flipx)
                                if(flipy == t.flipy)
                                    return true;
        }


        return false;
    }


    //runs once per frame
    public void render(SpriteBatch batch, float delta, World world){
        batch.draw(texture, x, y);
    }

    public void render(SpriteBatch batch, float x, float y){
        batch.draw(texture, x, y);
    }
}
