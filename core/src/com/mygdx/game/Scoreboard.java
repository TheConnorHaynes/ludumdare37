package com.mygdx.game;

import com.badlogic.gdx.utils.Array;

public class Scoreboard {

    int successes;
    int failures;
    int victoryCount;
    int defeatCount;
    public boolean victory;
    public boolean defeat;

    public Scoreboard(int victoryCount, int defeatCount) {

        this.successes = 0;
        this.failures = 0;
        this.victory = false;
        this.defeat = false;
        this.victoryCount = victoryCount;
        this.defeatCount = defeatCount;

    }

    public void succeed() {
        successes++;
        if (successes >= victoryCount)
            victory = true;
    }

    public void fail() {
        failures++;
        if (failures >= defeatCount)
            defeat = true;
    }
}