package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Connor on 12/12/2016.
 */
public class HeartButton extends Button{
    TextureRegion sprite;
    TextureRegion[][] splitSheet;
    float time;
    Animation animation;
    float animEnd;
    public HeartButton(int x, int y) {
        super(x, y);
        width = 30;
        height = 32;
        time = 0;
        splitSheet = new TextureRegion(new Texture("organbutton.png")).split(64,64);
        animation = new Animation(.1f, splitSheet[0]);
        animEnd = .4f;
    }

    public void interact(){
        MainGame.world.beating = false;
    }

    public void render(SpriteBatch batch, float delta){
        if(time>animEnd)
            time = 0;
        if(MainGame.world.beating){
            time+=delta*2;
        }
        else
            time+=delta;
        sprite = animation.getKeyFrame(time);
        batch.draw(sprite, x-20, y-32);
    }
}
