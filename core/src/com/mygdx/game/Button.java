package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Connor on 12/10/2016.
 */
public class Button extends Tile {
    String text;
    Color color;
    Sound press;
    Sound bad;
    public Button(int x, int y){
        super(x,y,0,0,false,false);
        text = "";
        color = Color.WHITE;
        press = Gdx.audio.newSound(Gdx.files.internal("Sounds/ButtonPress.wav"));
        bad = Gdx.audio.newSound(Gdx.files.internal("Sounds/BadButton.wav"));
    }

    public void setText(String s, Color c){
        text = s;
        color = c;
    }

    public float collidex(MovingObject o){
        float col = super.collidex(o);
        if(o instanceof Interact && col !=0){
            interact();
        }
        return col;
    }

    public void render(SpriteBatch batch, float delta){
        MainGame.smallerFont.setColor(color);
        MainGame.smallerFont.draw(batch, text, x +width/2 - text.length()*3.75f, y);
        MainGame.smallerFont.setColor(Color.WHITE);
    }

    public void interact(){
        if(MainGame.dialogue!=null && !MainGame.world.win && !MainGame.world.lose) {
            if (!text.equals("") && MainGame.world.frogs.size == 0 && (MainGame.world.butterflies.size == 0 || MainGame.world.butterflies.get(0).caged) && !MainGame.world.beating) {
                MainGame.scrollingText.setText(MainGame.dialogue.takeAction(text));
                MainGame.setButtons();
                if (!MainGame.muted) {
                    press.play();
                }
            } else if (!MainGame.muted) {
                bad.play();
            }

            MainGame.world.timeBeep = 1;
            MainGame.world.time = 7;
            MainGame.world.ticking = false;
        }
    }

    public void interactForce(){
        MainGame.scrollingText.setText(MainGame.dialogue.takeAction(text));
        MainGame.setButtons();
        if(!MainGame.muted) {
            bad.play();
        }
    }
}
