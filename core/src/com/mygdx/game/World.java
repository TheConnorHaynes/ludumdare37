package com.mygdx.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Connor on 5/10/2016.
 */
public class World {
    public Texture back;
    public Texture front;
    public Texture top;
    public Array<Tile> foreground;
    public Array<MovingObject> moving;
    public Player player;
    boolean texturesSet;
    public int element;
    public float time;
    public Date date;
    public Array<Frog> frogs;
    public Array<Button> buttons;
    public Array<Butterfly> butterflies;
    public boolean beating;
    public boolean ticking;
    public float timeBeep;
    public Sound beep;
    public boolean win;
    public boolean lose;
    public Texture winHeart;
    public Texture loseHeart;


    public World(){
        winHeart = new Texture("winheart.png");
        loseHeart = new Texture("loseheart.png");
        win = false;
        lose = false;
        beating = false;
        beep = Gdx.audio.newSound(Gdx.files.internal("Sounds/MenuDing.wav"));
        top = new Texture("upperStomach.png");
        buttons = new Array<Button>();
        buttons.add(new Button(250, 335));
        buttons.add(new Button(370, 335));
        buttons.add(new Button(490, 335));
        buttons.add(new HeartButton(450, 150));
        back = new Texture("brain.png");
        front = new Texture("organs.png");
        time = 5;
        element = 1;
        player = new Player();
        foreground = new Array<Tile>();
        moving = new Array<MovingObject>();
        foreground.add(new ConsoleCollision(230, 335));
        for(Button b: buttons){
            foreground.add(b);
        }
        date = new Date();
        frogs = new Array<Frog>();
        butterflies = new Array<Butterfly>();
    }

    public void updatePhysics(){
        for(MovingObject o : moving)
            o.updatePhysics();
        player.updatePhysics();
        for(Frog frog : frogs)
            frog.updatePhysics();
        for(Butterfly b : butterflies)
            b.updatePhysics();
    }

    public void reset(){
        frogs.clear();
        butterflies.clear();
        ticking = false;
        time = 7;
        timeBeep = 0;
    }

    public void render(SpriteBatch batch, float delta){
        if(ticking){
            time -= delta;
            timeBeep -= delta;
            if(timeBeep<=0) {
                if(!MainGame.muted)
                    beep.play();
                timeBeep = 1;
            }
            if(time<=0){
                timeBeep = 0;
                if(!buttons.get(0).text.equals("")){
                    buttons.get(0).interactForce();
                }
                else if(!buttons.get(1).text.equals("")){
                    buttons.get(1).interactForce();
                }
                else if(!buttons.get(2).text.equals("")){
                    buttons.get(2).interactForce();
                }
                ticking = false;
                time = 7;
            }

        }
        date.render(batch, delta);
        batch.draw(back, 0, 0);
        if(!texturesSet) {
            for (Tile tile : foreground)
                tile.setTexture();
            texturesSet = true;
        }
        for(Frog frog : frogs)
            frog.render(batch,delta,this);
        for(MovingObject o : moving)
            o.render(batch, delta, this);
        for(Tile tile : foreground)
            tile.render(batch, delta, this);
        batch.draw(front, 0, 0);
        for(Button b : buttons){
            b.render(batch, delta);
        }

        player.render(batch, delta, this);
        for(Butterfly b : butterflies)
            b.render(batch, delta, this);
        batch.draw(top, 0,0);
        if(win){
            batch.draw(winHeart, 0, 0);
        }
        else if(lose)
            batch.draw(loseHeart, 0, 0);
    }



    public void releaseButterflies(){
        for(Butterfly b : butterflies)
            b.jettison();
    }

    public World clone(){
        World r = new World();
        r.foreground = foreground;
        return r;
    }

    public void frogEruption(){
        int i = (int)(Math.random()*10+6);
        while(i>0){
            frogs.add(new Frog(350,30));
            i--;
        }
    }

    public void butterflyEruption(){
        int i = (int)(Math.random()*10+6);
        while(i>0){
            butterflies.add(new Butterfly());
            i--;
        }
    }

    public void end(boolean b){
        if(b){
            win = true;
        }
        else{
            lose = true;
        }
        reset();
    }


}
