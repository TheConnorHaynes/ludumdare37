package com.mygdx.game;

import com.badlogic.gdx.utils.Array;
import java.util.HashMap;

public class Node {

    public int id;
    public String text;
    HashMap<String, Choice> choices;
    Personality personality;

    public Node(int id, String text, Array<Choice> choiceList, Personality personality) {
        this.id = id;
        this.text = text;
        this.choices = new HashMap<String, Choice>();
        for (Choice choice : choiceList) {
            choices.put(choice.action, choice);
        }
        this.personality = personality;
    }

    public Array<String> getActionsText() {
        Array<String> actions = new Array<String>();
        for (String action : choices.keySet()) {
            actions.add(action);
        }
        return actions;
    }

    public Tuple<Integer, Integer> takeAction(String action) {
        System.out.println("node choice: " + choices + " : chooses: " + action);
        return personality.takeAction(action, choices.get(action));
    }

}