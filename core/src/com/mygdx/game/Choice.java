package com.mygdx.game;

import com.badlogic.gdx.utils.Array;

public class Choice {

    public String action;
    int like;
    int dislike;

    public Choice(String action, int like, int dislike) {

        this.action = action;
        this.like = like;
        this.dislike = dislike;

    }
}