package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by hcmg_local on 12/11/2016.
 */
public class ScrollingText {
    private float x;
    private float y;
    private String text;
    private final float SPEED = 150;

    public ScrollingText(){
        y = 430;
        x = 800;
        text = "";
    }

    public void setText(String s){
        text =s;
        x = 800;
    }

    public void render(float delta, SpriteBatch batch, BitmapFont font){
        x-=SPEED*delta;
        if(x < -text.length()*14.5f) {
            x = 800;
            if(MainGame.world.win || MainGame.world.lose){
                MainGame.dialogue = new Dialogue();
                MainGame.menu = true;
                MainGame.world.win = false;
                MainGame.world.lose = false;
                MainGame.world.reset();
            }
            else
                MainGame.world.ticking = true;
        }
        font.draw(batch, text, x, y);
    }
}
