package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * Created by hcmg_local on 12/10/2016.
 */
public class Date {

    FrameBuffer fbo;

    float x;
    float y;
    float time;
    Array<Integer> face;
    TextureRegion[][] matrix;
    Texture finishedFace;
    TextureRegion f;


    public ShaderProgram crt;

    public ShaderProgram normal;

    public ShaderProgram hair;

    public Date(){
        time = 0;

        fbo = new FrameBuffer(Pixmap.Format.RGB888, 800, 600, false);
        System.out.println( MainGame.batch.getShader().getFragmentShaderSource());
        normal = new ShaderProgram(MainGame.batch.getShader().getVertexShaderSource(), MainGame.batch.getShader().getFragmentShaderSource());
        crt = new ShaderProgram(MainGame.batch.getShader().getVertexShaderSource(), Gdx.files.internal("crt.fsh").readString());
        hair = new ShaderProgram(MainGame.batch.getShader().getVertexShaderSource(), Gdx.files.internal("hair.fsh").readString());
        System.out.println(crt.getLog());
        face = new Array<Integer>();
        for(int i = 0; i < 6; i++){
            face.add((int)(Math.random()*6));
        }
        x = 336;
        y = 460;
        matrix = new TextureRegion(new Texture("faces.png")).split(128,128);
        float r = (float)Math.random();
        float g = (float)Math.random();
        float b = (float)Math.random();

        while(r < .1 && g<.1 && b<.1){
            r = (float)Math.random();
            g = (float)Math.random();
            b = (float)Math.random();
        }



        //TODO make a shader for this
        MainGame.batch.begin();

        MainGame.batch.setShader(hair);
        hair.setUniformf("hair", r, g, b);
        fbo.begin();
        MainGame.batch.draw(matrix[1][face.get(0)], 0, 0);
        MainGame.batch.draw(matrix[0][face.get(1)], 0, 0);
        MainGame.batch.draw(matrix[2][face.get(2)], 0, 0);
        MainGame.batch.draw(matrix[3][face.get(3)], 0, 0);
        MainGame.batch.draw(matrix[4][face.get(4)], 0, 0);
        MainGame.batch.draw(matrix[5][face.get(5)], 0, 0);
        MainGame.batch.end();
        fbo.end();
        MainGame.batch.setShader(normal);
        finishedFace = fbo.getColorBufferTexture();
        f = new TextureRegion(finishedFace,0,0,128,128);
        f.flip(false,true);

    }

    public void render(SpriteBatch batch, float delta){

        time += delta;
        batch.setShader(crt);
        batch.draw(f, x, y);
        crt.setUniformf("iGlobalTime", time/18);

        batch.setShader(normal);
    }

    public void render(){
        MainGame.batch.setShader(hair);
        MainGame.batch.draw(f, 0, 0);
        MainGame.batch.setShader(normal);
    }
}
