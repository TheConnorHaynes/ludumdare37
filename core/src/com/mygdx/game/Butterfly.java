package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by hcmg_local on 12/11/2016.
 */
public class Butterfly extends MovingObject {
    Animation animation;
    TextureRegion[][] sheet;
    float time;
    public boolean caged;
    float drag;
    Sound caught;
    public Butterfly(){
        super(25,25,32,32);
        caught = Gdx.audio.newSound(Gdx.files.internal("Sounds/butterflypoof.wav"));
        sheet = new TextureRegion(new Texture("butterfly.png")).split(32,32);
        animation = new Animation(.1f,sheet[0]);
        time = 0;
        caged = true;
        speed = 100/60f;
        drag = 1/60f;
    }

    public void updatePhysics(){
           if(caged){
               move();
            if(x +width > 150)
                x = 150-width;
            if(x < 10)
                x = 10;
            if(y<70)
                y = 70;
            if(y+height > 180)
                y = 180-height;
        }
        else{
               super.updatePhysics();
           }

        if(dx > 0){
            if(dx<drag)
                dx=0;
            else
                dx -= drag;
        }
        if(dx < 0){
            if(dx>drag)
                dx=0;
            else
                dx += drag;
        }
        if(dy > 0){
            if(dy<drag)
                dy=0;
            else
                dy -= drag;
        }
        if(dy < 0){
            if(dy>drag)
                dy=0;
            else
                dy += drag;
        }
        if(dx == 0 && dy ==0)
            flutter();
    }
    public void move(){
        x+=dx;
        y-=dy;
    }

    public void flutter(){
        dx = (float)(Math.random()-.5f)*speed;
        dy = (float)(Math.random()-.5f)*speed;
    }

    public void jettison(){
        caged = false;
        dx = (float)(Math.random())*speed*3;
        dy = (float)(Math.random()-.5f)*speed*3;
    }

    public void render(SpriteBatch batch, float delta, World world){
        time+=delta;
        if(collides(world.player) && !caged) {
            world.butterflies.removeValue(this, true);
            if(!MainGame.muted)
                caught.play(.5f);
        }
        if(time > .4f)
            time = 0;
        batch.draw(animation.getKeyFrame(time), x, y);
    }

    public boolean equals(Object o){
        if( o instanceof Butterfly && ((Butterfly) o).x == x && ((Butterfly) o).y==y)
            return true;
        return false;
    }
}
