package com.mygdx.game;

import com.badlogic.gdx.utils.Array;

public class ConversationStarter extends Conversation {

    public String segway;
    Array<String> actions;

    public ConversationStarter(String segway, Array<String> actions) {
        super(null, 0, new Array<Node>(), null);
        this.segway = segway;
        this.actions = actions;
    }

    public Node getNode(int id) {
        return null;
    }

    public Array<String> getActions() {
        return actions;
    }

    public Tuple<String, Integer> takeAction(String action) {
        if (action != null) {
            //get next random conversation of the correct type
            return new Tuple<String, Integer>(action, 2);
        } else {
            //if take aciton == null
            return new Tuple<String, Integer>(segway, 1);
        }
    }

}
