package com.mygdx.game;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.Gdx;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.ArrayList;

public class Dialogue {

    public Scoreboard scoreboard;

    Array<Array<String>> actions = new Array<Array<String>>();
    HashMap<String, Array<Conversation>> conversations = new HashMap<String, Array<Conversation>>();
    Random rand;

    public Conversation currConvo;

    String rootText;
    Array<String> rootOptions = new Array<String>();

    Array<String> likeSegways;
    Array<String> dislikeSegways;

    Array<String> victoryPhrases;
    Array<String> defeatPhrases;

    Personality personality;

    public Dialogue() {
        scoreboard = new Scoreboard(7, 7);

        rand = new Random();

        likeSegways = new Array<String>();
        likeSegways.add("Well that was interesting!");
        likeSegways.add("I'm glad that I came out tonight, I'm having a great time.");
        likeSegways.add("Segways are wierd, right?");
        likeSegways.add("Have you heard the news recently, something about lobsters I think.");
        dislikeSegways = new Array<String>();
        dislikeSegways.add("Um, yeah. Anyways.");
        dislikeSegways.add("That's great... Can we please talk about something else now?");
        dislikeSegways.add("Is there anything else you enjoy? Anything?");
        dislikeSegways.add("*Awkwardly looks at phone*");
        dislikeSegways.add("Oh yeah, that's... That's great...");

        defeatPhrases = new Array<String>();
        defeatPhrases.add("Of course I talk like an idiot, how else would you understand me?");
        defeatPhrases.add("Mirrors can’t talk and lucky for you they can’t laugh.");
        defeatPhrases.add("Your are as useless as a traffic light in Grand Theft Auto!");
        defeatPhrases.add("You are about as useful as a vibrater with no batteries!");
        defeatPhrases.add("Oh I get it.  That was like a joke, except not funny.");
        defeatPhrases.add("Save your breath, you will need it to blow up your date tonight.");
        defeatPhrases.add("They say opposites attract, I hope you meet someone good looking and intelligent.");
        defeatPhrases.add("Let’s play horse.  I’ll be the front and you be yourself.");
        defeatPhrases.add("Are you always this stupid or are you trying harder today?");
        defeatPhrases.add("People like you are why we have middle fingers.");
        defeatPhrases.add("Keep talking, some day you may say something intelligent.");
        defeatPhrases.add("I wish they made a vaccine to cure your stupidity.");
        defeatPhrases.add("Your brain must be like new, since you never use it.");

        victoryPhrases = new Array<String>();
        victoryPhrases.add("Marry me!");
        victoryPhrases.add("I think I found the one!");
        victoryPhrases.add("I found my diamond in the rough!");
        victoryPhrases.add("Oh baby, I think I wanna marry you!");

        JsonReader jReader = new JsonReader();
        FileHandle dirFile = Gdx.files.internal("conversations/directory.json");
        JsonValue json = jReader.parse(dirFile);

        //parse action groups
        JsonValue actionsJson = json.get("actions");
        for (JsonValue group : actionsJson) {
            Array<String> newGroup = new Array<String>();
            for (JsonValue action : group) {
                newGroup.add(action.asString());
            }
            actions.add(newGroup);
        }

        personality = new Personality(actions);

        //find conversation files
        JsonValue convoFilesJson = json.get("conversations");
        for (JsonValue filePath : convoFilesJson) {
            FileHandle convoFile = Gdx.files.internal("conversations/" + filePath.asString());
            JsonValue convoJson = jReader.parse(convoFile);

            JsonValue action = convoJson.get("action");
            JsonValue startNode = convoJson.get("start_node");

            JsonValue nodesJson = convoJson.get("nodes");
            Array<Node> nodes = new Array<Node>();
            for (JsonValue jNode : nodesJson) {
                JsonValue id = jNode.get("id");
                JsonValue text = jNode.get("text");

                JsonValue choiceJson = jNode.get("choices");
                Array<Choice> choices = new Array<Choice>();
                for (JsonValue jChoice : choiceJson) {
                    JsonValue choiceAction = jChoice.get("action");
                    JsonValue choiceNodes = jChoice.get("nodes");
                    JsonValue like = choiceNodes.get("like");
                    JsonValue dislike = choiceNodes.get("dislike");
                    choices.add(new Choice(choiceAction.asString(), like.asInt(), dislike.asInt()));
                }
                nodes.add(new Node(id.asInt(), text.asString(), choices, personality));
            }
            Conversation convo = new Conversation(action.asString(), startNode.asInt(), nodes, personality);
            if (conversations.containsKey(convo.action)) {
                conversations.get(convo.action).add(convo);
            } else {
                Array<Conversation> temp = new Array<Conversation>();
                temp.add(convo);
                conversations.put(convo.action, temp);
            }
        }
    }

    /*
    starts next conversation
    returns true or false depending on whether conversation succeeded or not
     */
    public String startNextConversation() {
        if (rootText == null) {
            //first conversation
            rootText = "Hi";
        } else {
            rootText = "I'm not supposed to be saying this...";
        }
        //randomly select prompt

        //randomly select 3 conversation action starters
//        rootOptions.add("Flirt");
//        rootOptions.add("Brag");
//        rootOptions.add("Tell Joke");
        Array<String> randActions = getRandomActions();
        currConvo = new ConversationStarter(rootText, randActions);

        //randomly pick a conversation that is of the correct action type
//        currConvo = conversations.get("Flirt").get(rand.nextInt(3));
//        currConvo = conversations.get("Tell Joke").get(1);
        return rootText;
    }

    public Array<String> getActions() {
        return currConvo.getActions();
    }

    public String takeAction(String action) {
        System.out.println("dialogue starting takeAction: " + action);
        Tuple<String, Integer> response = currConvo.takeAction(action);
        System.out.println(response.x + " : " + response.y);
        if (response.y == 1) {
            //add butterfly
            scoreboard.succeed();
            if (scoreboard.victory) {
                System.out.println("VICTORY");
//                MainGame.scrollingText.setText("test1");
                response = new Tuple<String, Integer>(victoryPhrases.get(rand.nextInt(victoryPhrases.size)), response.y);
                MainGame.world.end(true);
            }
            MainGame.world.butterflyEruption();
        } else if (response.y == 0) {
            //add frogs
            scoreboard.fail();
            if (scoreboard.defeat) {
                System.out.println("DEFEAT");
//                MainGame.scrollingText.setText("test2");
                response = new Tuple<String, Integer>(defeatPhrases.get(rand.nextInt(defeatPhrases.size)), response.y);
                MainGame.world.end(false);
            }
            MainGame.world.frogEruption();
        }
        if (rand.nextFloat() <= .1) {
            MainGame.world.releaseButterflies();
        }
        if(rand.nextFloat() < .05)
            MainGame.world.beating = true;
        if (response.x == null) {
            //end old conversation
            //set currConvo to a ConversationStarter
            System.out.println("CHANGING TO CONVO STRTER");
            if (response.y == 1) {
                //success
                Array<String> randActions = getRandomActions();
                currConvo = new ConversationStarter(likeSegways.get(rand.nextInt(likeSegways.size)), randActions);
            } else {
                //fail
                Array<String> randActions = getRandomActions();
                currConvo = new ConversationStarter(dislikeSegways.get(rand.nextInt(dislikeSegways.size)), randActions);
            }
            return currConvo.takeAction(response.x).x;
        } else if (response.y == 2) {
            System.out.println("changing to the new convo based on: " + response.x);
            currConvo = conversations.get(response.x).get(rand.nextInt(conversations.get(response.x).size));
            System.out.println(currConvo);
            return currConvo.currNode.text;
        } else {
            System.out.println("taking action: " + action);
            return response.x;
        }
    }

    public String getRandomAction() {
//        Array<String> randGroup = actions.get(rand.nextInt(actions.size));
//        return randGroup.get(rand.nextInt(randGroup.size));
        Set<String> keys = conversations.keySet();
        int index = rand.nextInt(keys.size());
        int i = 0;
        for (String key : keys) {
            if (i == index)
                return key;
            i++;
        }
        return null;
    }

    public Array<String> getRandomActions() {
        Set<String> keySet = conversations.keySet();
        Object[] temps = keySet.toArray();

        Array<String> keys = new Array<String>();
        Array<String> output = new Array<String>();

        for (Object temp : temps) {
            keys.add((String) temp);
        }

        for (int k = 0; k < 3; k++) {
            int index = rand.nextInt(keys.size);
            for (int i = 0; i < keys.size; i++) {
                if (i == index) {
                    output.add(keys.get(i));
                    keys.removeValue(keys.get(i), true);
                }
            }
        }
        return output;
    }

}
